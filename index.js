// ---------------------------------------------------------------------------
//  mime-types
//
//  bare bones mime type checker and resolver
// ---------------------------------------------------------------------------

    var forOwn = require('@amphibian/for-own');
    var inArray = require('@amphibian/in-array');

    var types = {
        'text/plain': [
            'text/plain',
            'plain',
            'text'
        ],
        'text/html': [
            'text/html',
            'html'
        ],
        'text/css': [
            'text/css',
            'css'
        ],
        'application/javascript': [
            'application/javascript',
            'text/javascript',
            'javascript',
            'js'
        ],
        'application/json': [
            'application/json',
            'json'
        ],
        'application/x-www-form-urlencoded': [
            'application/x-www-form-urlencoded',
            'urlencoded',
            'form'
        ]
    };

    function mimeTypes(type, comparison) {
        if (!type) {
            throw new Error('mime-types: At least 1 function argument is required');
        }

        if (type) { type = type.toLowerCase(); }
        if (comparison) { comparison = comparison.toLowerCase(); }

        if (type && !comparison) {
            return forOwn(types, function (key, value, end) {
                if (inArray(type, value)) {
                    end(key);
                }
            });
        } else if (type && comparison) {
            return mimeTypes(type) === mimeTypes(comparison);
        }
    }

    module.exports = mimeTypes;
