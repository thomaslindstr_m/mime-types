# mime-types

[![build status](https://gitlab.com/ci/projects/30668/status.png?ref=master)](https://gitlab.com/ci/projects/30668?ref=master)

bare bones mime type checker and resolver

```
npm install @amphibian/mime-types
```

```javascript
var mimeTypes = require('@amphibian/mime-types');

mimeTypes('json'); // > application/json

var randomMimeType = 'text/html';
mimeTypes(randomMimeType, 'html'); // > true
mimeTypes(randomMimeType, 'css'); // > false
```
