// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var mimeTypes = require('./index.js');
    var assert = require('assert');

    describe('mime-types', function() {
        const _htmlMimeType = 'text/html';

        it('should return the html mime type', function () {
            assert.equal(mimeTypes('html'), _htmlMimeType);
        });

        it('should validate the html mime type', function () {
            assert.equal(mimeTypes(_htmlMimeType, 'html'), true);
        });

        const _cssMimeType = 'text/css';

        it('should return the css mime type', function () {
            assert.equal(mimeTypes('css'), _cssMimeType);
        });

        it('should validate the css mime type', function () {
            assert.equal(mimeTypes(_cssMimeType, 'css'), true);
        });

        const _javascriptMimeType = 'application/javascript';

        it('should return the js mime type', function () {
            assert.equal(mimeTypes('js'), _javascriptMimeType);
        });

        it('should validate the js mime type', function () {
            assert.equal(mimeTypes(_javascriptMimeType, 'js'), true);
        });

        const _jsonMimeType = 'application/json';

        it('should return the json mime type', function () {
            assert.equal(mimeTypes('json'), _jsonMimeType);
        });

        it('should validate the json mime type', function () {
            assert.equal(mimeTypes(_jsonMimeType, 'json'), true);
        });

        const _urlencodedMimeType = 'application/x-www-form-urlencoded';

        it('should return the urlencoded mime type', function () {
            assert.equal(mimeTypes('urlencoded'), _urlencodedMimeType);
        });

        it('should validate the urlencoded mime type', function () {
            assert.equal(mimeTypes(_urlencodedMimeType, 'urlencoded'), true);
        });

        it('should return false on checking against unmatching mime types', function () {
            assert.equal(mimeTypes(_urlencodedMimeType, 'html'), false);
        })

        it('should accept any letter case', function () {
            assert.equal(mimeTypes(_urlencodedMimeType, 'URLENCODED'), true);
        });
    });
